import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets
import pandas as pd
import seaborn as sns
import sklearn.cluster as clstr
from sklearn.cluster import KMeans


def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                                    centers=4,
                                    cluster_std=[1.0, 2.5, 0.5, 3.0],
                                    random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

#X = generate_data(500, 1)

for i in range(1,6):
    X = generate_data(500, i)
    inertias = []
    
    #for clusters in range(1,20):
     #   kmeans = clstr.KMeans(clusters).fit(X)
      #  inertias.append(kmeans.inertia_)

kmeans = KMeans(n_clusters=20)
kmeans.fit(X)
y_kmeans = clstr.KMeans.predict(X)
#print(inertias)

#plt.figure(1)

plt.scatter(X[:, 0], X[:, 1], c='g', s=20, cmap='turbo')

centers = kmeans.cluster_centers_
plt.scatter(centers[:, 0], centers[:, 1], c='black', s=50, alpha=0.5)

inertia = []
for i in range(1,20):
    model=clstr.KMeans(n_clusters=i)
    model.fit(X)
    inertia.append([i, model.inertia_])

plt.plot(inertia)

plt.show()