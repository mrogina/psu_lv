


ocjena = input("Upisi svoju ocjenu:")

try:
    ocjena = float(ocjena)
except:
    print("Unesi broj")

if(ocjena >= 0.9 and ocjena < 1.0):
    print("A")
elif(ocjena >= 0.8 and ocjena < 0.9):
    print("B")
elif(ocjena >= 0.7 and ocjena < 0.8):
    print("B")
elif(ocjena >= 0.6 and ocjena < 0.7):
    print("C")
elif(ocjena >= 0.5 and ocjena < 0.6):
    print("D")
elif(ocjena < 0.5 and ocjena > 0.0 ):
    print('PAD')
else:
    print("Ocjena nije u rasponu")
