
from math import inf

count = 0
sum = 0.0
min = inf
max = -inf

while (True):
    data = input("Unos: ")

    if (data == "Done"):
        break
    else:
        try:
            flt = float(data)
        except:
            print("Not a number!")
            continue
        
        if (flt < min):
            min = flt
        if (flt > max):
            max = flt

        sum += flt
        count += 1

print("Avg:", sum / count)
print("Min", min)
print("Max", max)
