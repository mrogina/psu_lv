import numpy as np 
import matplotlib as ml
import matplotlib.pyplot as plt

a = []

for i in range(100):
    a.append(np.random.randint(1,7))

plt.hist(a, bins=range(1,8), align="left", edgecolor="silver")

plt.show()

