import numpy as np 
import matplotlib as ml
import matplotlib.pyplot as plt

data = np.loadtxt(open("mtcars.csv", "rb"), usecols=(1,2,3,4,5,6), delimiter=",", skiprows=1)

wr = data[:,5]


#print(data)
plt.scatter(data[:,0], data[:,3], linewidths=wr)

#plt.scatter()
plt.xlabel("mpg")
plt.ylabel("hp")
plt.show()

