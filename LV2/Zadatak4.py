import matplotlib.pyplot as plt
import numpy as np
import skimage.io
img = skimage.io.imread('tiger.png', as_gray=True)
#img_new=img.copy()
img_nova=img.copy()
rows, cols=img.shape
img_nova=np.zeros((cols, rows))

#for i in range (rows):
#    for j in range(cols):
#        if img[i][j]>220:
#            img_new[i][j]=255
#        else:
#            img_new[i][j]+=50

for i in range (rows):
    for j in range (cols):
        img_nova[j, i]=img[i,j]

plt.figure(1)
plt.imshow(img, cmap='gray', vmin=0, vmax=255)
plt.figure(2)
plt.imshow(img_nova, cmap='gray', vmin=0, vmax=255)
plt.show()
